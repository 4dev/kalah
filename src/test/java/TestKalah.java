import org.junit.Assert;
import org.junit.Test;
import za.co.backbaseproject.Board;
import za.co.backbaseproject.GameEngine;
import za.co.backbaseproject.Player;

/**
 * Created by renier on 2017/03/28.
 */
public class TestKalah {

    /**
     * Test if capturing the opponents stones work according to the rules
     */
    @Test
    public void testCapture() {

        Player [] players = new Player[2];
        int [] northPlayerStones = {2, 0, 0, 6, 6, 6};
        int [] southPlayerStones = {6, 6, 6, 6, 6, 6};

        players[0] = new Player(GameEngine.NORTH_PLAYER);
        players[1] = new Player(GameEngine.SOUTH_PLAYER);

        players[0].setStones(northPlayerStones);
        players[1].setStones(southPlayerStones);

        Board board = GameEngine.initGame();
        board.setWhosTurn(GameEngine.NORTH_PLAYER);
        board.setPlayers(players);
        GameEngine.setNorthPlayer(players[0]);
        GameEngine.setSouthPlayer(players[1]);

        board = GameEngine.playPit(board.getPlayers()[0]);

        Assert.assertEquals(7, board.getPlayers()[0].getScore());
    }

    /**
     * Test if score increments on first go
     * as well as who's turn it should be
     */
    @Test
    public void testScoring() {
        Board board = GameEngine.initGame();
        board.setWhosTurn(GameEngine.NORTH_PLAYER);
        board = GameEngine.playPit(board.getPlayers()[0]);

        Assert.assertEquals(1, GameEngine.getNorthPlayer().getScore());
        Assert.assertEquals("north_player", board.getWhosTurn());
    }

}
