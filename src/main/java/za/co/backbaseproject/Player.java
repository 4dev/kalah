package za.co.backbaseproject;


/**
 * Created by renier on 2017/03/26.
 */
public class Player {

    private String name;
    // The amount of stones in the kalah
    private int score;
    // All the pits for the player
    private int[] stones = new int[6];
    private int selectedPit;

    public Player() {}

    public Player(String name) {
        this.name = name;

        for (int i = 0; i < 6; i++) {
            stones[i] = 6;
        }
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public int[] getStones() {
        return stones;
    }

    public void setStones(int[] stones) {
        this.stones = stones;
    }

    public int getSelectedPit() {
        return selectedPit;
    }

    public void setSelectedPit(int selectedPit) {
        this.selectedPit = selectedPit;
    }
}
