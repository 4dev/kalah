package za.co.backbaseproject;


/**
 * Created by renier on 2017/03/27.
 */
public class Board {

    private Player[] players;
    private String whosTurn;

    public Board(Player northPlayer, Player southPlayer) {
        players = new Player[2];
        players[0] = northPlayer;
        players[1] = southPlayer;
    }

    public Player[] getPlayers() {
        return players;
    }

    public void setPlayers(Player[] players) {
        this.players = players;
    }

    public String getWhosTurn() {
        return whosTurn;
    }

    public void setWhosTurn(String whosTurn) {
        this.whosTurn = whosTurn;
    }
}
