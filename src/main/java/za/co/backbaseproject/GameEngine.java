package za.co.backbaseproject;

import org.apache.commons.lang3.ArrayUtils;

/**
 * Created by renier on 2017/03/26.
 */
public class GameEngine {

    public static final String NORTH_PLAYER = "north_player";
    public static final String SOUTH_PLAYER = "south_player";

    private static final int PIT_COUNT = 6;

    private static Player northPlayer;
    private static Player southPlayer;
    private static Board board;

    /**
     * Starting a fresh game
     * @return Brand spanking new board
     */
    public static Board initGame() {
        northPlayer = new Player(NORTH_PLAYER);
        southPlayer = new Player(SOUTH_PLAYER);
        board = new Board(northPlayer, southPlayer);
        board.setWhosTurn(NORTH_PLAYER);
        return board;
    }

    /**
     * Let the games begin
     * @param player The current playing player
     * @return The board after play
     */
    public static Board playPit(Player player) {

        //No one should be cheating, making sure it's this players turn
        if (board.getWhosTurn().equals(player.getName())) {
            if (player.getName().equals(northPlayer.getName())) {
                startSowing(northPlayer, southPlayer, player.getSelectedPit());
            } else {
                startSowing(southPlayer, northPlayer, player.getSelectedPit());
            }
        }
        return board;
    }

    /**
     * This is where all the magic happens
     * @param playingPlayer The good guy
     * @param opposingPlayer The enemy
     * @param selectedPit The pit selected by the player
     */
    private static void startSowing(Player playingPlayer, Player opposingPlayer, int selectedPit) {

        // Get the count of stones in the selected pit
        int stoneCount = playingPlayer.getStones()[selectedPit];

        // There is no stone ...
        if (stoneCount > 0) {

            // Set the pit stone count to 0 because we are sowing them all over :)
            playingPlayer.getStones()[selectedPit] = 0;

            // We start sowing the stones from the selected pit ...
            stoneCount = sowIntoPlayerPits(selectedPit + 1, stoneCount, playingPlayer, opposingPlayer, stoneCount < PIT_COUNT);

            // ... and we continue for the opponents pits as well
            stoneCount = sowIntoOpponentsPits(stoneCount, opposingPlayer);

            // Whilst we have stones
            while (stoneCount > 0) {
                stoneCount = sowIntoPlayerPits(0, stoneCount, playingPlayer, opposingPlayer, stoneCount < PIT_COUNT);
                stoneCount = sowIntoOpponentsPits(stoneCount, opposingPlayer);
            }
        }
    }

    /**
     * Sow stones into players pits (Sounds horrible)
     * @param startPosition From where to start sowing
     * @param stoneCount Amount of stones to sow
     * @param playingPlayer Friendly guy
     * @param opposingPlayer That other guy
     * @param lastStone Is the last stone in batch (Maybe we can capture some stones)
     * @return The stones that are left
     */
    private static int sowIntoPlayerPits(int startPosition, int stoneCount, Player playingPlayer, Player opposingPlayer, boolean lastStone) {
        int tmpStoneCount = stoneCount;
        for (int i = startPosition; i <= tmpStoneCount; i++) {

            if (i == PIT_COUNT) {
                playingPlayer.setScore(playingPlayer.getScore() + 1);
                board.setWhosTurn(playingPlayer.getName());
                stoneCount--;
                break;
            }

            if (lastStone && i == tmpStoneCount) {

                if (playingPlayer.getStones()[i] == 0) {
                    playingPlayer.getStones()[i] = 0;
                    playingPlayer.setScore(playingPlayer.getScore() + 1);
                    captureOpponentStones(i, playingPlayer, opposingPlayer);
                    stoneCount--;
                }
                break;
            }
            playingPlayer.getStones()[i]++;
            stoneCount--;
        }
        return stoneCount;
    }

    /**
     * Fill up the opponents pit as well
     * @param stoneCount The stones to sow
     * @param opposingPlayer Enemy!!!
     * @return The stones that are left
     */
    private static int sowIntoOpponentsPits(int stoneCount, Player opposingPlayer) {
        int tmpStoneCount = stoneCount;
        if (stoneCount > 0) {
            board.setWhosTurn(opposingPlayer.getName());
            tmpStoneCount = stoneCount;
            for (int i = 0; i < tmpStoneCount; i++) {
                if (i == PIT_COUNT) {
                    break;
                }
                opposingPlayer.getStones()[i]++;
                stoneCount--;
            }
        }
        return stoneCount;
    }

    /**
     * Capture those stones
     * @param pitPosition Position to start sowing to
     * @param playingPlayer
     * @param opposingPlayer
     */
    private static void captureOpponentStones(int pitPosition, Player playingPlayer, Player opposingPlayer) {
        // Reverse the opposing players stones
        ArrayUtils.reverse(opposingPlayer.getStones());
        int oppositionStones = opposingPlayer.getStones()[pitPosition];
        opposingPlayer.getStones()[pitPosition] = 0;
        playingPlayer.setScore(playingPlayer.getScore() + oppositionStones);
        // Reverse them back
        ArrayUtils.reverse(opposingPlayer.getStones());
    }

    public static Board getBoard() {
        return board;
    }

    public static Player getNorthPlayer() {
        return northPlayer;
    }

    public static void setNorthPlayer(Player northPlayer) {
        GameEngine.northPlayer = northPlayer;
    }

    public static Player getSouthPlayer() {
        return southPlayer;
    }

    public static void setSouthPlayer(Player southPlayer) {
        GameEngine.southPlayer = southPlayer;
    }
}