package za.co.backbaseproject;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;


/**
 * Created by renier on 2017/03/26.
 */
@Path("/")
public class KalahResource {

    /**
     * Start a new game
     * @return New board object
     */
    @GET
    @Path("/startgame")
    @Produces(MediaType.APPLICATION_JSON)
    public Board startgame() {
        return GameEngine.initGame();
    }

    /**
     * Get the current board
     * @return
     */
    @GET
    @Path("/getboard")
    @Produces(MediaType.APPLICATION_JSON)
    public Board getboard() {
        return GameEngine.getBoard() == null ? GameEngine.initGame() : GameEngine.getBoard();
    }

    /**
     * Play the game
     * @param player Player object, the one whos turn it is
     * @return
     */
    @POST
    @Path("/sowstones")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Board sowstones(Player player) {

        // A new game will be created if one does not exist
        if (GameEngine.getBoard() == null) {
            GameEngine.initGame();
        }
        return GameEngine.playPit(player);
    }
}
