/**
 * Created by renier on 2017/03/28.
 */
angular.module('kalahGame', [])
    .controller('KalahGameController', function($scope, $http) {

        $scope.players = []
        $scope.board = {};

        $scope.postClick = function (player, index) {
            player.selectedPit = index;

            var req = {
                method: 'POST',
                url: '/kalah/sowstones',
                headers: {
                    'Content-Type': 'application/json'
                },
                data: player
            }

            $http(req).then(function(response){
                $scope.board = response.data;
                $scope.players = $scope.board.players;
            });
        };

        $http.get("/kalah/startgame")
            .then(function(response) {
                $scope.board = response.data;
                $scope.players = $scope.board.players;
            });

    });
